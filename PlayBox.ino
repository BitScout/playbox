#include <Adafruit_NeoPixel.h>

// Dinge, die sich nie ändern:
#define LEDS_PIN      6 // Anschluss des LED-Rings
#define POTI_A_PIN   A0 // Anschluss von Potentiometer A
#define POTI_B_PIN   A1 // Anschluss von Potentiometer B
#define SCHALTER_PIN 3 // Anschluss des LED-Rings
#define PIEPSER_PIN  9 // Anschluss des Piezo-Lautsprechers
#define MOTOR_PIN    10 // Anschluss des Vibrationsmotor
#define LEDS_ANZAHL  16 // Anzahl der LEDs im LED-Ring
#define HELLIGKEIT   10 // 0 - 255
#define BLINKFREQUENZ 1500 // Wie lange es dauern soll bis die LED des blinkenden Spielers wieder an geht (seit dem letzten Mal)

// Unser LED-Streifen muss wissen, wo er angeschlossen ist:
Adafruit_NeoPixel ledRing = Adafruit_NeoPixel(LEDS_ANZAHL, LEDS_PIN, NEO_GRB + NEO_KHZ800);

// Farben der beiden Spieler
uint32_t farbe_A = ledRing.Color(0, HELLIGKEIT, 0);
uint32_t farbe_B = ledRing.Color(0, 0, HELLIGKEIT);

float position_A;
float position_B;

float geschwindigkeit_A;
float geschwindigkeit_B;

int potentiometer_A;
int potentiometer_B;
bool schalter;
bool schalterVorher;

float hoechstgeschwindigkeit = 0.1;

int faenger = 0; // 0 steht für Spieler A, 1 für Spieler B
int runden = 7;  // Danach endet das Spiel

void setup(){
  
  // Welche Anschlüsse als Sensoren (Eingänge) benutzt werden und welche als Ausgänge:
  pinMode(POTI_A_PIN, INPUT);
  pinMode(POTI_B_PIN, INPUT);
  pinMode(SCHALTER_PIN, INPUT);
  pinMode(PIEPSER_PIN, OUTPUT); // Anschluss für den Piezo-Lautsprecher setzen
  pinMode(MOTOR_PIN, OUTPUT);   // Anschluss für den Vibrationsmotor setzen
  
  ledRing.begin(); // LED-Ring starten

  startaufstellung(); // Spieler auf ihre Startpositionen
}

// Das hier wird ständig wiederholt
void loop(){
  
  // Sensoren auslesen:
  potentiometer_A = (potentiometer_A * 2 + analogRead(POTI_A_PIN)) / 3;
  potentiometer_B = (potentiometer_B * 2 + analogRead(POTI_B_PIN)) / 3;
  schalter = digitalRead(SCHALTER_PIN);
  
  if(schalter) {
    glitzern();
    
    return;
  } else if(schalterVorher) {
    startaufstellung();
    runden = 7;
  }
  
  schalterVorher = schalter; // Merken, wo der Schalter vorher war
  
  // Spiel ggf. beenden
  if(runden <= 0) {
    spielerAusblenden();
    ledRing.show();
    delay(1000);
    return;
  }

  // Geschwindigkeit der Spieler berechnen:
  geschwindigkeit_A = (potentiometer_A / 1024.0 - 0.5) * hoechstgeschwindigkeit;
  geschwindigkeit_B = (potentiometer_B / 1024.0 - 0.5) * hoechstgeschwindigkeit;

  spielerAusblenden();
  pruefeObGefangen();
  spielerPositionenAktualisieren();
  spielerAnzeigen();
  ledRing.show(); // Erst mit diesem Befehl wird auf dem LED-Ring wirklich etwas angezeigt
  
  delay(25); // Ganz kurz warten
}

static void startaufstellung() {
  position_A = 2;
  position_B = 11;
}

// Schaltet die LEDs aus, auf denen die Spieler gerade stehen:
static void spielerAusblenden() {
  ledRing.setPixelColor(position_A, 0);
  ledRing.setPixelColor(position_B, 0);
}

// Ändert die Positionen der beiden Spieler:
static void spielerPositionenAktualisieren() {
  
  position_A += geschwindigkeit_A;
  position_B += geschwindigkeit_B;
  
  if(position_A >= LEDS_ANZAHL) {
    position_A -= LEDS_ANZAHL;
  }
  
  if(position_B >= LEDS_ANZAHL) {
    position_B -= LEDS_ANZAHL;
  }
  
  if(position_A <= 0) {
    position_A += LEDS_ANZAHL;
  }
  
  if(position_B <= 0) {
    position_B += LEDS_ANZAHL;
  }
}

// Pruefen ob jemand gefangen wurde und den Sieger anzeigen
static void pruefeObGefangen() {

  // Wenn die beiden Spieler nicht auf der selben Position sind brauchen wir nicht weiter zu machen
  if((int) position_A != (int) position_B) {
    return; 
  }

  // Farbe des Fängers anzeigen
  if(faenger == 0) {
    ringEinfaerben(farbe_A);
  } else {
    ringEinfaerben(farbe_B);
  }
  
  ledRing.show();
  tone(PIEPSER_PIN, 500);      // Piepsen anschalten
  analogWrite(MOTOR_PIN, 255); // Motor anschalten
  delay(200);
  noTone(PIEPSER_PIN);         // Piepsen stoppen
  analogWrite(MOTOR_PIN, 0);   // Motor stoppen
  delay(3000);
  ringEinfaerben(0); // Alle LEDs aus
  ledRing.show();

  // Der Gefangene wird jetzt zum Fänger
  faenger = (faenger + 1) % 2;
  startaufstellung();

  // Geschwindigkeit um 20% erhöhen und Runde zählen
  hoechstgeschwindigkeit *= 1.2;
  runden--;
}

// Schaltet die LEDs dort an wo sich die Spieler gerade befinden:
static void spielerAnzeigen() {
  
  if(faenger == 1 || millis() % BLINKFREQUENZ < BLINKFREQUENZ / 2) {
    ledRing.setPixelColor(position_A, farbe_A);
  }
  
  if(faenger == 0 || millis() % BLINKFREQUENZ < BLINKFREQUENZ / 2) {
    ledRing.setPixelColor(position_B, farbe_B);
  }
}

static void ringEinfaerben(uint32_t farbe) {
  for(int i=0; i < LEDS_ANZAHL; i++) {
    ledRing.setPixelColor(i, farbe);
  }
}

static void glitzern() {
  int glitzerHelligkeit = 150;
  double poti = potentiometer_A / 1024.0;
  double glitzerWahrscheinlichkeit = potentiometer_B / 1024.0 + 0.05;
  int rotAnteil = 0;
  int gruenAnteil = 0;
  int blauAnteil = 0;

  if(poti < 2.0/6.0) {
    rotAnteil = (0.25 - abs(poti - 1.0/12.0)) * glitzerHelligkeit;
  } else if (poti > 5.0/6.0) {
    rotAnteil = (0.25 - abs(poti - 13.0/12.0)) * glitzerHelligkeit;
  }
  if(poti > 1.0/6.0 && poti < 5.0/6.0) {
    gruenAnteil = (0.25 - abs(poti - 5.0/12.0)) * glitzerHelligkeit;
  }
  if(poti > 0.5) {
    blauAnteil = (0.25 - abs(poti - 9.0/12.0)) * glitzerHelligkeit;
  }

  rotAnteil = max(rotAnteil, 0);
  gruenAnteil = max(gruenAnteil, 0);
  blauAnteil = max(blauAnteil, 0);

  for(int i=0; i < LEDS_ANZAHL; i++) {
    if(random(500) / 100.0 < glitzerWahrscheinlichkeit) {
      float variation = 0.25 + random(100) / 100.0 * 0.75;
      ledRing.setPixelColor(i, ledRing.Color(rotAnteil * variation, gruenAnteil * variation, blauAnteil * variation));
    }
  }
  
  ledRing.show();
  delay(20);
}

